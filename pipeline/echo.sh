
#export COUNT=$(aws dynamodb scan --table-name SlicerVersion --select "COUNT")
#echo $COUNT

export Value=$(aws dynamodb scan --region $aws_region --table-name $table_name)
echo $Value >currentValueCmd.json

export COUNT=$(cat currentValueCmd.json | jq .Count)
if [[ $COUNT -eq 0 ]]
then
   echo "Table SlicerVersion is empty"
else 
   echo "Table SlicerVersion contains $COUNT items"
fi

export currentValueCmd=$(aws dynamodb get-item --region $aws_region --table-name $table_name --key "{\"VersionID\":{\"S\":\"LATEST\"}}")
echo $currentValueCmd 

if [[ $currentValueCmd -eq " " ]]
then
echo "Key does not exist in table SlicerVersion, creating it..."
aws dynamodb put-item --region $aws_region --table-name $table_name --item "{\"VersionID\":{\"S\":\"LATEST\"},\"DockerTag\":{\"S\":\"$DockerTag\"},\"SlicerVersion\":{\"S\":\"$SlicerVersion\"},\"SlicerVersionCreationTime\":{\"S\":\"$SlicerVersionCreationTime\"}}" --condition-expression "attribute_not_exists(VersionID)"
echo createditem
else
echo "Key exist in table SlicerVersion, updating it..."
aws dynamodb update-item --region $aws_region --table-name $table_name --key "{\"VersionID\":{\"S\":\"LATEST\"}}" --attribute-updates "{\"DockerTag\":{\"Action\":\"PUT\",\"Value\":{\"S\":\"$DockerTag\"}}},{\"SlicerVersion\":{\"Action\":\"PUT\",\"Value\":{\"S\":\"$SlicerVersion\"}}},{\"SlicerVersionCreationTime\":{\"Action\":\"PUT\",\"Value\":{\"S\":\"$SlicerVersionCreationTime\"}}} "
echo updateditem
fi






# export dynamolist=$(aws dynamodb get-item --region $aws_region --table-name $tableName --key "{\"VersionID\":{\"S\":\"1\"}}")
# echo $dynamolist
