#!/usr/bin/perl

use warnings;
use JSON;
use Data::Dumper;

my $awsRegion = shift;
my $tableName = shift;
my $keyName = shift;
my $keyValue = shift;
my $attributeName = shift;
my $attributeVal = shift;



my $items = get_number_of_items($tableName);
print "Table $tableName contains $items items\n";
if ($items == 0) {
    print "Table $tableName is empty\n";
    return;
}

my $currentValueCmd = "aws dynamodb get-item --table-name=$tableName --key '{\"$keyName\":{\"S\":\"$keyValue\"}}' --region $awsRegion";
print "currentValueCmd=[$currentValueCmd]\n";

my $currentValue = `$currentValueCmd`;
print $currentValue."\n";

if ($currentValue eq "") {
    print "Key does not exist in table $tableName, creating it...\n";
    my %item = ();
    $item{$keyName}{'S'} = $keyValue;
    open OUT, ">item.json";
    print OUT encode_json(\%item);
    close OUT;

    my $createItemCmd = "aws dynamodb put-item --region $awsRegion --table-name $tableName --item file://item.json --condition-expression \"attribute_not_exists($keyName)\"";
    print "createItemCmd=[$createItemCmd]\n";
    my $result = `$createItemCmd`;
    print "Result:\n";
    print $result."\n";

}

my $updateValueCmd = "aws dynamodb update-item --table-name=$tableName --key '{\"$keyName\":{\"S\":\"$keyValue\"}}' --attribute-updates '{\"$attributeName\": {\"Action\": \"PUT\",\"Value\": {\"S\":\"$attributeVal\"}}}' --region $awsRegion";
print "updateValueCmd=[$updateValueCmd]\n";
my $updatedValue = `$updateValueCmd`;
print "Result:\n";
print $updatedValue."\n";


sub get_number_of_items {
    my $tableName = shift;

    my $cmd = "aws dynamodb scan --table-name $tableName --select \"COUNT\" --region $awsRegion";
    print "Issuing command [$cmd]\n";
    my $content = `$cmd`;

    my $contentObject = eval { decode_json($content) };
    if ($@) {
        print "Unable to decode JSON\n";
        return 0;
    }

    return $$contentObject{'Count'};
}
