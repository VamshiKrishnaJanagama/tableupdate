FROM nvidia/cuda:10.2-runtime-ubuntu18.04
WORKDIR /opt/uplynk/slicer

RUN apt-get update && \
    apt-get install libidn11 && \
    apt-get install -y wget
        
# -- enable for python 2.7 --
# COPY requirments.txt .
# RUN pip install -r requirments.txt

# -- enable for python 3.6 --
# COPY requirments36.txt .
# RUN pip3 install -r requirments.txt

ARG archive 
